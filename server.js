const { createServer } = require('http')
const next = require('next')
const routes = require('./routes')
const mobxReact = require('mobx-react')

const dev = process.env.NODE_ENV !== 'production'
const port = parseInt(process.env.PORT, 10) || 3000
const app = next({ dev })
const handler = routes.getRequestHandler(app)

mobxReact.useStaticRendering(true)

app.prepare()
.then(() => {
  createServer(handler)
  .listen(3000, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})