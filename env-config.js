const prod = process.env.NODE_ENV === 'production'

module.exports = {
  'BACKEND_URL': prod ? 'http://128.199.65.193:8080/rest' : 'http://localhost:8080/rest'
}