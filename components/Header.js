import { Link } from '~/routes'
import React from 'react'
import NProgress from 'nprogress'
import Router from 'next/router'


const linkStyle = {
  marginRight: 15
}

Router.onRouteChangeStart = (url) => {
  console.log(`Loading: ${url}`)
  NProgress.start()
}
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

export default () => (
  <div>
    <Link route="home">
      <a style={linkStyle}>Home</a>
    </Link>
    <Link route="about">
      <a style={linkStyle}>About</a>
    </Link>
  </div>
)