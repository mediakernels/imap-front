import { action, observable, computed, toJS } from 'mobx';

export default new class AuthStore {
    
    storageId = "authStore7HUGGy6Tg895YGhfJtffgtsoplqu786gGu878jsdfjhd7h99";
    
    @observable username = "";
    @observable token = "";
    @observable expired = "";
    @observable features = [];

    isLoaded = false;

     @action clearDto(){
        this.setProps({
            username:"",
            token:"",
            expired:"",
            features:[]
        })
    }

    @action setProps(props = {}) {
        for (let key in props) {
            this[key] = props[key]
        }
    }

    @computed get isAuth() {
        // return this.username.length > 0 && this.token.length>30 &&
        // (new Date()).getTime()<this.expired;
        return true;
    }

    hasAccessFeature(feature){
        // if(feature==undefined){
        //     return true;
        // }
        // return this.isAuth && this.features.indexOf(feature)>-1;
        return true;
    }

    @action clear() {
        this.clearDto();
        this.saveToStorage();
    }

    @action save(authDto) {
        this.setProps(authDto);
        this.saveToStorage();
    }

    saveToStorage() {
        localStorage.setItem(this.storageId, JSON.stringify(this.dto));
    }

    @action loadFromStorage(){
        let authDto = JSON.parse(localStorage.getItem(this.storageId));
        if(authDto==undefined){
            this.clearDto();
        }else{
            this.setProps(authDto)
        }
    }

    @computed get dto() {
        let propKeys = ["username", "token", "expired","features"];
        return this.getDtoByPropKeys(propKeys)
    }

    getDtoByPropKeys(propKeys = []) {
        let result = {};
        propKeys.map((key) => {
            result[key] = this[key];
        })
        return result;
    }

}