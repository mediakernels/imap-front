import React from 'react';
import { observer } from "mobx-react";
import { Link } from '~/routes'


@observer
export default class LayoutLogin extends React.Component {
    render() {
        return (
            <div className="login">
                <div className="menu-toggler sidebar-toggler"></div>
                <div className="logo">
                    <Link route="home">
                        <img src="/static/image/logo-ooredoo.png"/> 
                    </Link>
                </div>
                <div className="content">
                   {this.props.children}
                </div>
            </div>
        )
    }
}