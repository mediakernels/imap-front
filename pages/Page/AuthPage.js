import React from 'react';
import {action} from 'mobx';
import { observer } from "mobx-react";
import Store from "./PageStore"

@observer
export default class AuthPage extends React.Component {

    componentDidMount(){
        this.loadAndCheckAuth();
    }

    componentDidUpdate(){
        this.loadAndCheckAuth();
    }

    @action loadAndCheckAuth(){
        /* execute only on client*/
        if(typeof window != undefined){
            Store.loadAuthFromStorage();
            Store.isAuthOrToLogin()
        }
    }

    render() {
        let result = null;

        if (Store.isAuth ) {
            result = this.props.children;
        }

        return (
            <div>{result}</div>
        )
    }
}
