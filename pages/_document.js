import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

export default class MyDocument extends Document {
    static getInitialProps({ renderPage }) {
        const { html, head, errorHtml, chunks } = renderPage()
        const styles = flush()
        return { html, head, errorHtml, chunks, styles }
    }

    render() {
        return (
            <html>
                <Head>
                    <meta charSet="utf-8" />
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                    <meta content="width=device-width, initial-scale=1" name="viewport" />
                    <link rel="icon" href="/static/uredu.ico" type="image/x-icon" />
                    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
                    <link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
                    <link href="/static/global/css/components-rounded.min.css" rel="stylesheet" type="text/css" />
                    <link href="/static/layout6/css/layout.min.css" rel="stylesheet" type="text/css" />
                    <link rel='stylesheet' type='text/css' href='/static/nprogress.css' />
                    <link href="/static/animate.css" rel="stylesheet" type="text/css" />
                    <link rel='stylesheet' type='text/css' href='/static/login.min.css' />
                    <link href="/static/layout6/css/custom.min.css" rel="stylesheet" type="text/css" />
                    <link href="/static/react-date-picker.css" rel="stylesheet" type="text/css" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        )
    }
}