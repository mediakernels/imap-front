import Progress from 'nprogress'
import { Router } from '../routes'


let nProgress = new class NProgress {
    progressCount = 0;

    start() {
        if (this.progressCount == 0) {
            Progress.start();
        }
        this.progressCount += 1;
    }

    done() {
        if (this.progressCount > 0) {
            this.progressCount -= 1;
        }
        if (this.progressCount == 0) {
            Progress.done();
        }
    }
}

Router.onRouteChangeStart = (url) => {
    nProgress.start();
}
Router.onRouteChangeComplete = () => nProgress.done()
Router.onRouteChangeError = () => nProgress.done();

export default nProgress;