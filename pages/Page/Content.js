import Breadcrumb from "./Breadcrumb";
import React from 'react';
import { observer } from "mobx-react";

@observer
export default class Content extends React.Component {
    render() {
        const children = this.props.children;
        return (
            <div className="container-fluid">
                <div className="page-content">
                    {/*<div className="page-content-fixed-header">
                        <Breadcrumb />
                    </div>*/}
                    <div className="page-fixed-main-content">
                        {children}
                    </div>
                    <div className="copyright-v2">
                        2017 &copy; Indosat Ooredoo. All Rights Reserved.
                </div>
                </div>
            </div>
        )
    }
}
