import { action, observable,computed } from 'mobx';
import { Router } from '~/routes';
import AuthStore from "~/stores/AuthStore";

export default new class PageStore {
    appName = "Ooredoo";

    @observable title = "Ooredoo";
    @observable layout = "default"
    @observable access = [];
    @observable breadcrumb = [];

    @action setProps(props = {}) {
        for (let key in props) {
            if (key == "title") {
                props[key] += " | " + this.appName;
            }
            this[key] = props[key]
        }
    }

    @action isAuthOrToLogin() {
        let result = true;
        if (!AuthStore.isAuth) {
            result = false;
            Router.pushRoute("login");
        }
        return result;
    }

    @action logout(){
        AuthStore.clear();
    }

    @computed get isAuth(){
        return AuthStore.isAuth;
    }

    hasAccessFeature(feature){
        return AuthStore.hasAccessFeature(feature);
    }

    @action loadAuthFromStorage(){
        AuthStore.loadFromStorage();
    }
}