import React from "react"
import { observer } from "mobx-react";
import Head from "next/head";

import LayoutDefault from "./LayoutDefault";
import LayoutLogin from "./LayoutLogin";

import Store from "./PageStore";

@observer
export default class Page extends React.Component {

  render() {
    let baseChildren = (
      <div>
         <Head>
          <title>{Store.title}</title>
        </Head>
          {this.props.children}
      </div>
    )
    let result = <LayoutDefault> {baseChildren}</LayoutDefault>
    if(Store.layout=="login"){
      result = <LayoutLogin> {baseChildren}</LayoutLogin>
    }
    return result;
  }
}