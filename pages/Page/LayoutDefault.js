import Header from './Header'
import Content from "./Content";
import React from "react"
import { observer } from "mobx-react";
import AuthPage from "./AuthPage";

@observer
export default class LayoutDefault extends React.Component {
    render() {
        return (
            <AuthPage>
                <div>
                    <Header />
                    <Content>
                        <div>
                            {this.props.children}
                        </div>
                    </Content>
                </div>
            </AuthPage>
        )
    }
}
