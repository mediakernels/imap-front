import React from 'react';
import { Link } from '~/routes'
import { observer } from "mobx-react";
import PageStore from "./PageStore";

@observer
export default class Breadcrumb extends React.Component {
    get store() {
        return PageStore;
    }
    render() {
        return (
            <ul className="page-breadcrumb">
                <li>
                    <Link route="home"><a>Home</a></Link>
                </li>
                {this.store.breadcrumb.map((item, key) => {
                    let label = item.label;
                    let route = item.route;
                    let params = item.params;
                    let result = (
                        <li key={key} className="active">
                            {label}
                        </li>
                    )

                    if (route != undefined) {
                        result = (
                            <li key={key}>
                                <Link route={route} params={params}><a>{label}</a></Link>
                            </li>
                        )
                    }
                    return result;
                })}
            </ul>
        )
    }
}
