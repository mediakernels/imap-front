import React from "react";
import AuthStore from "~/stores/AuthStore";

export default class AuthComp extends React.Component {
    get children() {
        return this.props.children || "";
    }

    get allowedFeatures() {
        return AuthStore.features || [];
    }

    get feature() {
        return this.props.feature || "";
    }

    get tag() {
        return this.props.tag || "span";
    }

    get parentProps() {
        let { feature, tag, ...props } = this.props;
        return props
    }

    get isVisible() {
        return this.allowedFeatures.indexOf(this.feature) > -1;
    }
    render() {
        return this.isVisible &&
            (<this.tag {...this.parentProps}>
                {this.children}
            </this.tag>)
    }
} 