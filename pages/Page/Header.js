import { Link } from '~/routes'
import { computed,observable,toJS } from "mobx";
import React from 'react';
import Head from "next/head"
import Store from "./PageStore";
import {observer} from "mobx-react"
import AuthComp from "~/components/AuthComp";
import { Dropdown } from 'react-bootstrap';

@observer
export default class Header extends React.Component {

  @observable headerMenu = [
    {
      name: "Report",
      icon: "line-chart",
      items: [{
        name: "Transaction Mobo",
        route: "general-report",
        feature: "TRANSACTION_REPORT"
      },{
        name: "KPI Canvasser",
        route: "trans-outlet-report",
        feature: "TRANS_OUTLET_REPORT",
      },{
        name: "Generate",
        route: "generate-reports",
        feature: "GENERATE_REPORT"
      }]
    }, {
      name: "Data",
      icon: "database",
      items: [{
        name: "Transaction Mobo",
        route: "trans-data",
        feature: "TRANSACTION_DATA",
      },{
        name: "Transaction Outlet",
        route: "trans-outlet-raw",
        feature: "TRANS_OUTLET_RAW_LIST",
      },{
        name: "Program Marketing",
        route: "program-marketing",
        feature: "PROGRAM_CATEGORY",
      }]
    }, {
      name: "Config",
      icon: "gear",
      items: [{
        name: "Users",
        route: "user",
        feature: "USER_LIST",

      }, {
        name: "Role",
        route: "role",
        feature: "ROLE_LIST",

      }, {
        name: "Logout",
        route: "login"
      }]
    }
  ]

  @computed get headerMenuWithAccess() {
    // clone array
    let result = toJS(this.headerMenu);
    return result.map((menu) => {
      menu.visible = false;
      menu.items = menu.items.map((item) => {
        item.visible = Store.hasAccessFeature(item.feature);
        menu.visible = menu.visible || item.visible;
        return item;
      })
      return menu;
    })
  }

  render() {
    return (
      <header className="page-header" >
        <nav className="navbar" role="navigation">
          <div className="container-fluid">
            <div className="havbar-header">
              <Link route="home">
                <a id="index" className="navbar-brand" >
                  <img src="/static/image/logo-ooredoo.png" alt="Logo" />
                </a>
              </Link>
              <div className="topbar-actions ">

                <Link route="home">
                  <button type="button" className="btn red-haze dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i className="fa fa-home"></i>
                    <span className="hidden-xs hidden-sm"> Home</span>
                  </button>
                </Link>
                {this.headerMenuWithAccess.map((menu, key) => {
                  return menu.visible && <Dropdown key={key} id={menu.name + "-dropdown"}>
                    <Dropdown.Toggle className="btn red-haze">
                      <i className={"fa fa-" + menu.icon}></i>
                      <span className="hidden-xs hidden-sm"> {menu.name}</span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {menu.items.map((item, key) => {
                        return item.visible && <li key={key}><Link route={item.route}><a>{item.name}</a></Link></li>;
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                })}
                <div className="btn-group-img btn-group">
                  {<a className="btn btn-sm" >
                    <img style={{width:0}}src="/static/layout6/img/avatar1.jpg" alt="" />
                  </a>}
                </div>
              </div>
            </div>
          </div>
        </nav>
      </header>
    )
  }
}
